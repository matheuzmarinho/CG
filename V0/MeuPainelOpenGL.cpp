#include "MeuPainelOpenGL.h"

MeuPainelOpenGL::MeuPainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{

}

void MeuPainelOpenGL::initializeGL()
{
qglClearColor(Qt::white);
}

void MeuPainelOpenGL::resizeGL(int w, int h)
{
glViewport(0, 0, w, h);
}

void MeuPainelOpenGL::paintGL()
{
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

glColor3f(1,0,0);
glBegin(GL_TRIANGLES);

    glVertex2f(-1,-1);
    glVertex2f(0,1);
    glVertex2f(1,-1);

glEnd();

}
