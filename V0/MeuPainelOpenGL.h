#ifndef MEUPAINELOPENGL_H
#define MEUPAINELOPENGL_H

#include <QWidget>
#include <QGLWidget>

class MeuPainelOpenGL : public QGLWidget
{
    Q_OBJECT
public:
    explicit MeuPainelOpenGL(QWidget *parent = 0);


protected:
    void initializeGL();
    void resizeGL(int w , int h);
    void paintGL();


signals:

public slots:
};

#endif // MEUPAINELOPENGL_H
