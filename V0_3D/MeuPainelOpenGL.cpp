#include "MeuPainelOpenGL.h"
#include "GL/glu.h"
MeuPainelOpenGL::MeuPainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{
    setFormat(QGL::DoubleBuffer | QGL::DepthBuffer);
    setFocusPolicy(Qt::StrongFocus);

    rotacaoX = 0;
}

void MeuPainelOpenGL::setRotacaoX(int valor)
{
    rotacaoX = valor;
}

void MeuPainelOpenGL::animacaoRotacaoX()
{

    int i,rotMax =  rotacaoX;
    int wait = 200;
    if(rotacaoX !=0){
        for(i=0;i<rotMax;i++){
            rotacaoX = i;
            glLoadIdentity();
            gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
            updateGL();
            Sleep(wait);
        }
    }
    else{
        glLoadIdentity();
        gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
        updateGL();
    }
}

void MeuPainelOpenGL::initializeGL()
{
    qglClearColor(Qt::white);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_LINE_SMOOTH_HINT, GL_SMOOTH);
}

void MeuPainelOpenGL::resizeGL(int w, int h)
{

    if(w>h){
        //caso a largura seja maior, o x deve começar na metade da difereça entre a largura e a altura.
        glViewport((w-h)/2, 0, h, h);
    }else{
        //caso a altura seja maior, o y deve começar na metade da difereça entre a altura e a largura  .
        glViewport(0, (h-w)/2, w, w);
    }

  // glViewport(0, 0, w, h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(15,w/h,0.1f,100);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
}

void MeuPainelOpenGL::paintGL()
{
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

glRotatef(rotacaoX,1,0,0);

glBegin(GL_QUADS);                // Begin drawing the color cube with 6 quads
      // Top face (y = 1.0f)
      // Define vertices in counter-clockwise (CCW) order with normal pointing out
      glColor3f(0.0f, 1.0f, 0.0f);     // Green
      glVertex3f( 1.0f, 1.0f, -1.0f);
      glVertex3f(-1.0f, 1.0f, -1.0f);
      glVertex3f(-1.0f, 1.0f,  1.0f);
      glVertex3f( 1.0f, 1.0f,  1.0f);

      // Bottom face (y = -1.0f)
      glColor3f(1.0f, 0.5f, 0.0f);     // Orange
      glVertex3f( 1.0f, -1.0f,  1.0f);
      glVertex3f(-1.0f, -1.0f,  1.0f);
      glVertex3f(-1.0f, -1.0f, -1.0f);
      glVertex3f( 1.0f, -1.0f, -1.0f);

      // Front face  (z = 1.0f)
      glColor3f(1.0f, 0.0f, 0.0f);     // Red
      glVertex3f( 1.0f,  1.0f, 1.0f);
      glVertex3f(-1.0f,  1.0f, 1.0f);
      glVertex3f(-1.0f, -1.0f, 1.0f);
      glVertex3f( 1.0f, -1.0f, 1.0f);

      // Back face (z = -1.0f)
      glColor3f(1.0f, 1.0f, 0.0f);     // Yellow
      glVertex3f( 1.0f, -1.0f, -1.0f);
      glVertex3f(-1.0f, -1.0f, -1.0f);
      glVertex3f(-1.0f,  1.0f, -1.0f);
      glVertex3f( 1.0f,  1.0f, -1.0f);

      // Left face (x = -1.0f)
      glColor3f(0.0f, 0.0f, 1.0f);     // Blue
      glVertex3f(-1.0f,  1.0f,  1.0f);
      glVertex3f(-1.0f,  1.0f, -1.0f);
      glVertex3f(-1.0f, -1.0f, -1.0f);
      glVertex3f(-1.0f, -1.0f,  1.0f);

      // Right face (x = 1.0f)
      glColor3f(1.0f, 0.0f, 1.0f);     // Magenta
      glVertex3f(1.0f,  1.0f, -1.0f);
      glVertex3f(1.0f,  1.0f,  1.0f);
      glVertex3f(1.0f, -1.0f,  1.0f);
      glVertex3f(1.0f, -1.0f, -1.0f);
   glEnd();  // End of drawing color-cube
   //glTranslatef(0,2,0);
glFlush();

}
