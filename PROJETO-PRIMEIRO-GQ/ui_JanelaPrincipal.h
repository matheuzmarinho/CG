/********************************************************************************
** Form generated from reading UI file 'JanelaPrincipal.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JANELAPRINCIPAL_H
#define UI_JANELAPRINCIPAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QSpinBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "meupainelopengl.h"

QT_BEGIN_NAMESPACE

class Ui_JanelaPrincipal
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    MeuPainelOpenGL *widget;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox;
    QSpinBox *btn_malha;
    QLabel *label;
    QSpinBox *x1;
    QSpinBox *y1;
    QSpinBox *x2;
    QSpinBox *y2;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QRadioButton *malha_box;
    QGroupBox *groupBox_2;
    QCheckBox *DDA;
    QCheckBox *checkBox;
    QCheckBox *checkBox_2;
    QCheckBox *checkBox_3;
    QPushButton *btn_sair;

    void setupUi(QMainWindow *JanelaPrincipal)
    {
        if (JanelaPrincipal->objectName().isEmpty())
            JanelaPrincipal->setObjectName(QString::fromUtf8("JanelaPrincipal"));
        JanelaPrincipal->resize(890, 580);
        centralWidget = new QWidget(JanelaPrincipal);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        widget = new MeuPainelOpenGL(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(widget);

        widget_2 = new QWidget(centralWidget);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(widget_2->sizePolicy().hasHeightForWidth());
        widget_2->setSizePolicy(sizePolicy1);
        widget_2->setMinimumSize(QSize(300, 0));
        widget_2->setMaximumSize(QSize(300, 16777215));
        verticalLayout_2 = new QVBoxLayout(widget_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox = new QGroupBox(widget_2);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        btn_malha = new QSpinBox(groupBox);
        btn_malha->setObjectName(QString::fromUtf8("btn_malha"));
        btn_malha->setGeometry(QRect(10, 50, 111, 20));
        btn_malha->setMinimum(1);
        btn_malha->setMaximum(99);
        btn_malha->setValue(10);
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 30, 47, 13));
        x1 = new QSpinBox(groupBox);
        x1->setObjectName(QString::fromUtf8("x1"));
        x1->setGeometry(QRect(10, 100, 120, 20));
        y1 = new QSpinBox(groupBox);
        y1->setObjectName(QString::fromUtf8("y1"));
        y1->setGeometry(QRect(150, 100, 120, 20));
        x2 = new QSpinBox(groupBox);
        x2->setObjectName(QString::fromUtf8("x2"));
        x2->setGeometry(QRect(10, 160, 120, 20));
        x2->setMinimum(0);
        y2 = new QSpinBox(groupBox);
        y2->setObjectName(QString::fromUtf8("y2"));
        y2->setGeometry(QRect(150, 160, 120, 20));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(10, 80, 47, 13));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 140, 47, 13));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(150, 80, 47, 13));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(150, 140, 47, 13));
        malha_box = new QRadioButton(groupBox);
        malha_box->setObjectName(QString::fromUtf8("malha_box"));
        malha_box->setEnabled(true);
        malha_box->setGeometry(QRect(150, 50, 82, 17));
        malha_box->setCheckable(true);
        malha_box->setChecked(true);

        verticalLayout->addWidget(groupBox);

        groupBox_2 = new QGroupBox(widget_2);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        DDA = new QCheckBox(groupBox_2);
        DDA->setObjectName(QString::fromUtf8("DDA"));
        DDA->setGeometry(QRect(10, 20, 70, 17));
        checkBox = new QCheckBox(groupBox_2);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setGeometry(QRect(60, 20, 81, 17));
        checkBox_2 = new QCheckBox(groupBox_2);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setGeometry(QRect(150, 20, 70, 17));
        checkBox_3 = new QCheckBox(groupBox_2);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setGeometry(QRect(220, 20, 91, 17));
        checkBox_3->setAcceptDrops(false);
        checkBox_3->setChecked(true);

        verticalLayout->addWidget(groupBox_2);

        btn_sair = new QPushButton(widget_2);
        btn_sair->setObjectName(QString::fromUtf8("btn_sair"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btn_sair->sizePolicy().hasHeightForWidth());
        btn_sair->setSizePolicy(sizePolicy2);

        verticalLayout->addWidget(btn_sair);


        verticalLayout_2->addLayout(verticalLayout);


        horizontalLayout->addWidget(widget_2);

        JanelaPrincipal->setCentralWidget(centralWidget);

        retranslateUi(JanelaPrincipal);
        QObject::connect(btn_sair, SIGNAL(clicked()), JanelaPrincipal, SLOT(close()));
        QObject::connect(btn_malha, SIGNAL(valueChanged(int)), widget, SLOT(alterarMalha(int)));
        QObject::connect(x1, SIGNAL(valueChanged(int)), widget, SLOT(setX1(int)));
        QObject::connect(x2, SIGNAL(valueChanged(int)), widget, SLOT(setX2(int)));
        QObject::connect(y1, SIGNAL(valueChanged(int)), widget, SLOT(setY1(int)));
        QObject::connect(y2, SIGNAL(valueChanged(int)), widget, SLOT(setY2(int)));
        QObject::connect(DDA, SIGNAL(toggled(bool)), widget, SLOT(desenharDDA()));
        QObject::connect(checkBox, SIGNAL(toggled(bool)), widget, SLOT(desenharPontoMedio()));
        QObject::connect(checkBox_2, SIGNAL(toggled(bool)), widget, SLOT(desenharExplicita()));
        QObject::connect(checkBox_3, SIGNAL(toggled(bool)), widget, SLOT(EsconderReta()));
        QObject::connect(malha_box, SIGNAL(toggled(bool)), widget, SLOT(MostrarMalha()));

        QMetaObject::connectSlotsByName(JanelaPrincipal);
    } // setupUi

    void retranslateUi(QMainWindow *JanelaPrincipal)
    {
        JanelaPrincipal->setWindowTitle(QApplication::translate("JanelaPrincipal", "JanelaPrincipal", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("JanelaPrincipal", "Coordenada", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("JanelaPrincipal", "MALHA", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("JanelaPrincipal", "X1", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("JanelaPrincipal", "X2", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("JanelaPrincipal", "Y1", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("JanelaPrincipal", "Y2", 0, QApplication::UnicodeUTF8));
        malha_box->setText(QApplication::translate("JanelaPrincipal", "Malha", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("JanelaPrincipal", "Algoritmos", 0, QApplication::UnicodeUTF8));
        DDA->setText(QApplication::translate("JanelaPrincipal", "DDA", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("JanelaPrincipal", "Ponto M\303\251dio", 0, QApplication::UnicodeUTF8));
        checkBox_2->setText(QApplication::translate("JanelaPrincipal", "Expl\303\255cipa", 0, QApplication::UnicodeUTF8));
        checkBox_3->setText(QApplication::translate("JanelaPrincipal", "Reta", 0, QApplication::UnicodeUTF8));
        btn_sair->setText(QApplication::translate("JanelaPrincipal", "SAIR", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class JanelaPrincipal: public Ui_JanelaPrincipal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JANELAPRINCIPAL_H
