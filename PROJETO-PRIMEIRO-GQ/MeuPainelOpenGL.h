#ifndef MEUPAINELOPENGL_H
#define MEUPAINELOPENGL_H

#include <QWidget>
#include <QGLWidget>

class MeuPainelOpenGL : public QGLWidget
{
    Q_OBJECT
public:
    explicit MeuPainelOpenGL(QWidget *parent = 0);


    int getMalha() const;
    void setMalha(int value);
    int getY1() const;
    int getX1() const;
    int getX2() const;
    int getY2() const;


protected:
    void initializeGL();
    void resizeGL(int w , int h);
    void paintGL();
     void retaDDA();
      void retaPM();
     void desenharPixel(int x, int y);
     int round(float numero);
     void retaEXP();
     void posicaoPontoMedio(float dx,float dy,bool negativo,float x, float y);
private:
    int malha;
    int x1;
    int y1;
    int y2;
    int x2;
    bool dda;
    bool pontoMedio;    
    bool explicita;
    bool reta;
    bool ExibirMalha;

    float maior(float x, float y);
    float menor(float x, float y);

signals:


public slots:    
    void setX1(int n);
    void setX2(int n);
    void setY1(int n);
    void setY2(int n);
    void alterarMalha(int malha);
    void desenharDDA();    
    void desenharPontoMedio();
    void desenharExplicita();
    void EsconderReta();
    void MostrarMalha();


};

#endif // MEUPAINELOPENGL_H
