/****************************************************************************
** Meta object code from reading C++ file 'MeuPainelOpenGL.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../MeuPainelOpenGL.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MeuPainelOpenGL.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MeuPainelOpenGL[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   17,   16,   16, 0x0a,
      30,   17,   16,   16, 0x0a,
      41,   17,   16,   16, 0x0a,
      52,   17,   16,   16, 0x0a,
      69,   63,   16,   16, 0x0a,
      87,   16,   16,   16, 0x0a,
     101,   16,   16,   16, 0x0a,
     122,   16,   16,   16, 0x0a,
     142,   16,   16,   16, 0x0a,
     157,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MeuPainelOpenGL[] = {
    "MeuPainelOpenGL\0\0n\0setX1(int)\0setX2(int)\0"
    "setY1(int)\0setY2(int)\0malha\0"
    "alterarMalha(int)\0desenharDDA()\0"
    "desenharPontoMedio()\0desenharExplicita()\0"
    "EsconderReta()\0MostrarMalha()\0"
};

void MeuPainelOpenGL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MeuPainelOpenGL *_t = static_cast<MeuPainelOpenGL *>(_o);
        switch (_id) {
        case 0: _t->setX1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->setX2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->setY1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->setY2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->alterarMalha((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->desenharDDA(); break;
        case 6: _t->desenharPontoMedio(); break;
        case 7: _t->desenharExplicita(); break;
        case 8: _t->EsconderReta(); break;
        case 9: _t->MostrarMalha(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MeuPainelOpenGL::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MeuPainelOpenGL::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_MeuPainelOpenGL,
      qt_meta_data_MeuPainelOpenGL, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MeuPainelOpenGL::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MeuPainelOpenGL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MeuPainelOpenGL::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MeuPainelOpenGL))
        return static_cast<void*>(const_cast< MeuPainelOpenGL*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int MeuPainelOpenGL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
