#include "MeuPainelOpenGL.h"
#include "GL/glu.h"
#include "MeuPainelOpenGL.h"
#include "math.h"

MeuPainelOpenGL::MeuPainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{
    malha = 10;
    x1=0;
    x2=0;
    y1=0;
    y2=0;
    dda = false;
    pontoMedio = false;
    explicita = false;
    reta = true;
    ExibirMalha = true;
}

void MeuPainelOpenGL::initializeGL()
{
qglClearColor(Qt::white);
}

void MeuPainelOpenGL::resizeGL(int w, int h)
{
    //descobre quem é o maior, altura ou largura
        if(w>h){
            //caso a largura seja maior, o x deve começar na metade da difereça entre a largura e a altura.
            glViewport((w-h)/2, 0, h, h);
        }else{
            //caso a altura seja maior, o y deve começar na metade da difereça entre a altura e a largura  .
            glViewport(0, (h-w)/2, w, w);
        }

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluOrtho2D(0, getMalha(), 0, getMalha());
   glMatrixMode(GL_MODELVIEW);
}

void MeuPainelOpenGL::paintGL()
{
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);




if((x1 != x2 || y1 != y2) && dda )
    retaDDA();

if((x1 != x2 || y1 != y2) && pontoMedio )
    retaPM();

if((x1 != x2 || y1 != y2) && explicita )
    retaEXP();

if((x1 != x2 || y1 != y2) && reta ){

    glColor3f(1.0,0.0,0.0);
    glBegin(GL_LINES);
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
    glEnd();

}

if(ExibirMalha){

    glColor3f(0.5,0.5,0.5);
    for(int x = 1; x<getMalha();x++){
        glBegin(GL_LINES);
         glVertex2i(x,getMalha());
         glVertex2i(x,0);
         glVertex2i(0,x);
         glVertex2i(getMalha(),x);
        glEnd();
    }
}

}
void MeuPainelOpenGL::retaEXP()
{
    float x, x0, xend, y0, yend;
    float y, m, b, maiorX, menorX, maiorY, menorY;

    maiorX = maior(x1,x2);
    maiorY = maior(y1,y2);
    menorX = menor(x1,x2);
    menorY = menor(y1,y2);

    x0 = x1;
    xend = x2;
    y0 = y1;
    yend = y2;
//coeficiente linear
        m = (yend - y0)/(xend - x0);
//termo independente
        b = y0 - (m * x0);

        // m > 1
    if(m > 1.0){
        for (y = menorY; y <= maiorY; y++) {
             x = (y - b)/m;
             glColor3f(0, 0.5f, 0); //cor do pixel verde
             desenharPixel(round(x),y);
        }
    }else{
        //0 < m < 1
        for (x = menorX; x <= maiorX; x++) {
                y = (m * x) + b;
                glColor3f(0, 0.5f, 0); //cor do pixel verde
                desenharPixel(x,round(y));
        }
    }
}



float MeuPainelOpenGL::maior(float x, float y){
    if(x > y)
        return x;
    return y;
}


float MeuPainelOpenGL::menor(float x, float y){
    if(x < y)
        return x;
    return y;
}

void MeuPainelOpenGL::retaDDA()
{
    int tamanho,i;
    float incrementoX,incrementoY,diferencaX,diferencaY,x,y;
    x = getX1();
    y = getY1();

    diferencaX = getX2() - getX1();
    diferencaY = getY2() - getY1();

    if( abs(diferencaX) > abs(diferencaY) ){
        tamanho =  abs(diferencaX);
    }else{
        tamanho = abs(diferencaY);
    }

    if(tamanho == 0){
        tamanho = 1;
    }

    incrementoX = diferencaX/tamanho;
    incrementoY = diferencaY/tamanho;

    glColor3f(0,0.7f,0.7f); // Define a cor de desenho
    for(i=0;i<=tamanho;i++){
        desenharPixel(round(x),round(y));
        x += incrementoX;
        y += incrementoY;
    }

}
void MeuPainelOpenGL::retaPM()
{
    float diferencaX, diferencaY,d,incE,incNE,x,y,x2,y2,aux,a,b;
    bool negativo = false;
    x = getX1();
    y = getY1();

    x2 = getX2();
    y2 = getY2();

    diferencaX = (x2 - x);
    diferencaY = (y2 - y);
    // descobre se é negativo e inverte o sinal dos y's
    if(diferencaX * diferencaY < 0){
        y = -y;
        y2 = -y2;
        diferencaY = -diferencaY;
        negativo = true;
    }

    //Define a inclinação da reta se dX < dY então a inclinação é >=1 então inverte os x's e y's
    if(abs(diferencaX) < abs(diferencaY)){
        aux = x;
        x = y;
        y = aux;
        aux =x2;
        x2 = y2;
        y2 = aux;
    }
    //Se o x > x2 então inverte a reta.
    if(x>x2){
        aux = x;
        x = x2;
        x2 = aux;
        aux = y;
        y=y2;
        y2=aux;
    }

    //descobre o a, b e o valor de decisão
    a = y2 - y;
    b = -x2 + x;
    d = 2*a + b;
    //calcula o incremento E e NE
    incE = 2*a;
    incNE = 2*(a + b);

    glColor3f(0, 0, 0); //cor preta

    posicaoPontoMedio(diferencaX,diferencaY,negativo,x,y);
    while(x < x2 ){
        if(d<=0){
            d += incE;
            x++;
        }else{
            d += incNE;
            x++;
            y++;
        }

        posicaoPontoMedio(diferencaX,diferencaY,negativo,x,y);

    }
}

void MeuPainelOpenGL::posicaoPontoMedio(float dx, float dy, bool negativo, float x, float y)
{
    //se a inclinação >=1 então inverte o x com o y
    if(abs(dx) < abs(dy)){
        if(negativo){
            desenharPixel(y,-x);
        }else{
            desenharPixel(y,x);
        }

    }else{
        if(negativo){
            desenharPixel(x,-y);
        }else{
            desenharPixel(x,y);
        }
    }
}


void MeuPainelOpenGL::desenharPixel(int x, int y)
{
    int lados = 40;
     float raio = 0.5;
     glLineWidth(1);
      glBegin(GL_POLYGON);
          for (int i = 0; i < lados; ++i)
              glVertex2f(x+ raio*cos(i*2*3.14159265/lados),
                         y+raio*sin(i*2*3.14159265/lados));
      glEnd();

}
int MeuPainelOpenGL::round(float numero)
{
    if (numero < 0) {
        numero -= 0.5;
    } else{
        numero += 0.5;
    }

    return (int) numero;
}
int MeuPainelOpenGL::getY2() const
{
    return y2;
}
int MeuPainelOpenGL::getX2() const
{
    return x2;
}
int MeuPainelOpenGL::getX1() const
{
    return x1;
}
int MeuPainelOpenGL::getY1() const
{
    return y1;
}
void MeuPainelOpenGL::setX1(int n)
{

    x1 = n;
    updateGL();
}
void MeuPainelOpenGL::setX2(int n)
{
    x2 = n;
    updateGL();
}
void MeuPainelOpenGL::setY1(int n)
{
    y1 = n;
    updateGL();
}
void MeuPainelOpenGL::setY2(int n)
{
    y2 = n;
    updateGL();
}
void MeuPainelOpenGL::alterarMalha(int malha)
{
    setMalha(malha);
    resizeGL(this->width(), this->height());
    updateGL();
}
void MeuPainelOpenGL::desenharDDA()
{
    dda = !dda;
    updateGL();
}
void MeuPainelOpenGL::desenharPontoMedio()
{
   pontoMedio = !pontoMedio;  
   updateGL();  
}
void MeuPainelOpenGL::desenharExplicita()
{
     explicita = !explicita;
     updateGL();
}
void MeuPainelOpenGL::EsconderReta()
{
     reta = !reta;
     updateGL();
}

void MeuPainelOpenGL::MostrarMalha()
{
    ExibirMalha = !ExibirMalha;
    updateGL();
}
int MeuPainelOpenGL::getMalha() const
{
    return malha;
}
void MeuPainelOpenGL::setMalha(int value)
{
    malha = value;
    updateGL();
}


