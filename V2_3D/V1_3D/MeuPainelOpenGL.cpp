#include "MeuPainelOpenGL.h"
#include "MeuPainelOpenGL.h"
#include "GL/glu.h"
#include <sstream>
#include <string>
MeuPainelOpenGL::MeuPainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{
    setFormat(QGL::DoubleBuffer | QGL::DepthBuffer);
    setFocusPolicy(Qt::StrongFocus);

    rotacaoX = 0;
    rotacaoY = 0;
    rotacaoZ = 0;

    translacaoX = 0;
    translacaoY = 0;
    translacaoZ = 0;

    rotacaoXObjeto = 0;
    rotacaoYObjeto = 0;
    rotacaoZObjeto = 0;

    escalaX = 1.0;
    escalaY = 1.0;
    escalaZ = 1.0;

    visaoX = 15;
    visaoY = 10;
    visaoZ = 15;

    CameraX = 0;
    CameraY = 0;
    CameraZ = 0;
    malha = 100;

    UpX = 0;
    UpY = 1;
    UpZ = 0;

    planoX = true;
    planoY = true;
    planoZ = true;

    tamanho = 2.0f;

    posicaoTransformacao = 0;
    posicaoAnimacao = -1;
}

int MeuPainelOpenGL::getVisaoX() const
{
    return visaoX;
}
int MeuPainelOpenGL::getVisaoY() const
{
    return visaoY;
}
int MeuPainelOpenGL::getVisaoZ() const
{
    return visaoZ;
}

int MeuPainelOpenGL::getCameraX() const
{
    return CameraX;
}
int MeuPainelOpenGL::getCameraY() const
{
    return CameraY;
}
int MeuPainelOpenGL::getCameraZ() const
{
    return CameraZ;
}

int MeuPainelOpenGL::getMalha() const
{
    return malha;
}

int MeuPainelOpenGL::getUpX() const
{
    return UpX;
}
int MeuPainelOpenGL::getUpY() const
{
    return UpY;
}
int MeuPainelOpenGL::getUpZ() const
{
    return UpZ;
}

float MeuPainelOpenGL::getTamanho() const
{
    return tamanho;

}

float MeuPainelOpenGL::getRotacaoY() const
{
    return rotacaoY;
}

void MeuPainelOpenGL::setRotacaoY(int valor)
{
    rotacaoY = valor;
}

void MeuPainelOpenGL::setTranslacaoZ(int valor)
{
    translacaoZ = valor;
}

void MeuPainelOpenGL::setTranslacaoY(int valor)
{
    translacaoY = valor;
}

void MeuPainelOpenGL::setTranslacaoX(int valor)
{
    translacaoX = valor;
}

void MeuPainelOpenGL::setEscalaX(double valor)
{
    escalaX = valor;
}

void MeuPainelOpenGL::setEscalaY(double valor)
{
    escalaY = valor;
}

void MeuPainelOpenGL::setEscalaZ(double valor)
{

    escalaZ = valor;
}

float MeuPainelOpenGL::getRotacaoZ() const
{
    return rotacaoZ;
}

bool MeuPainelOpenGL::getPlanoX() const
{
    return planoX;
}

void MeuPainelOpenGL::setPlanoX(bool value)
{
    planoX = value;
}

bool MeuPainelOpenGL::getPlanoY() const
{
    return planoY;
}

void MeuPainelOpenGL::setPlanoY(bool value)
{
    planoY = value;
}

bool MeuPainelOpenGL::getPlanoZ() const
{
    return planoZ;
}

void MeuPainelOpenGL::setPlanoZ(bool value)
{
    planoZ = value;
}

float MeuPainelOpenGL::getTranslacaoX() const
{
    return translacaoX;

}

float MeuPainelOpenGL::getTranslacaoY() const
{
    return translacaoY;
}

float MeuPainelOpenGL::getTranslacaoZ() const
{
    return translacaoZ;
}

float MeuPainelOpenGL::getEscalaX() const
{
    return escalaX;
}



float MeuPainelOpenGL::getEscalaY() const
{
    return escalaY;
}


float MeuPainelOpenGL::getEscalaZ() const
{
    return escalaZ;
}


void MeuPainelOpenGL::exibirTransformacao(QString transformacao)
{
    if(posicaoTransformacao == 0){
        emit setT1(transformacao);
    }else if(posicaoTransformacao == 1){
         emit setT2(transformacao);
    }else if(posicaoTransformacao == 2){
        emit setT3(transformacao);
   }else if(posicaoTransformacao == 3){
        emit setT4(transformacao);
   }else if(posicaoTransformacao == 4){
        emit setT5(transformacao);
   }
}

float MeuPainelOpenGL::getRotacaoX() const
{
    return rotacaoX;
}

void MeuPainelOpenGL::setRotacaoZ(int valor)
{
    rotacaoZ = valor;
}

void MeuPainelOpenGL::setTamanho(float valor)
{
    tamanho = valor;
}

void MeuPainelOpenGL::setCameraX(int valor)
{
    CameraX = valor;
    MoveVisao();
}
void MeuPainelOpenGL::setCameraY(int valor)
{
    CameraY = valor;
    MoveVisao();
}
void MeuPainelOpenGL::setCameraZ(int valor)
{
    CameraZ = valor;
    MoveVisao();
}

void MeuPainelOpenGL::setVisaoZ(int valor)
{
    visaoZ = valor;
    MoveVisao();
}
void MeuPainelOpenGL::setVisaoX(int valor)
{
    visaoX = valor;
     MoveVisao();
}
void MeuPainelOpenGL::setVisaoY(int valor)
{
    visaoY = valor;
     MoveVisao();
}

void MeuPainelOpenGL::setUpX(int valor)
{
    UpX = valor;
    MoveVisao();
}
void MeuPainelOpenGL::setUpY(int valor)
{
    UpY = valor;
    MoveVisao();
}
void MeuPainelOpenGL::setUpZ(int valor)
{
    UpZ = valor;
    MoveVisao();
}

void MeuPainelOpenGL::setRotacaoX(int valor)
{
    rotacaoX = valor;

}

void MeuPainelOpenGL::animacaoRotacaoX(int valor)
{
    int rotMax =  objeto.getRotacaoX() + valor;
    int wait = 50;

    for(int i = objeto.getRotacaoX();i<rotMax;i++){
        objeto.setRotacaoX(i);
        glLoadIdentity();
        gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
        //gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
        updateGL();
        Sleep(wait);
    }

}

void MeuPainelOpenGL::animacaoRotacaoY(int valor)
{
    int rotMax = objeto.getRotacaoY() + valor;
    int wait = 50;

    for(int i = objeto.getRotacaoY();i<rotMax;i++){
        objeto.setRotacaoY(i);
        glLoadIdentity();
        gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
        updateGL();
        Sleep(wait);
    }

}

void MeuPainelOpenGL::animacaoRotacaoZ(int valor)
{
    int rotMax =  objeto.getRotacaoZ() + valor;
    int wait = 50;
    for(int i = objeto.getRotacaoZ();i<rotMax;i++){
        objeto.setRotacaoZ(i);
        glLoadIdentity();
        gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
        updateGL();
        Sleep(wait);
    }


}

void MeuPainelOpenGL::animacaoTranslacao(float x, float y, float z)
{
    int XMax =  objeto.getTranslacaoX() + x;
    int YMax =  objeto.getTranslacaoY() + y;
    int ZMax =  objeto.getTranslacaoZ() + z;
    int wait = 100;
    for(int xi = objeto.getTranslacaoX();xi<=XMax;xi++){
        objeto.setTranslacaoX(xi);
        glLoadIdentity();
        gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
        updateGL();
        Sleep(wait);
    }
        for(int yi = objeto.getTranslacaoY();yi<=YMax;yi++){
            objeto.setTranslacaoY(yi);
            glLoadIdentity();
            gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
            updateGL();
            Sleep(wait);
        }
            for(int zi = objeto.getTranslacaoZ();zi<=ZMax;zi++){
                objeto.setTranslacaoZ(zi);
                glLoadIdentity();
                gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
                updateGL();
                Sleep(wait);
            }
}

void MeuPainelOpenGL::animacaoEscala(double x, double y, double z)
{
    double XMax =  objeto.getEscalaX() + x;
    double YMax =  objeto.getEscalaY() + y;
    double ZMax =  objeto.getEscalaZ() + z;
    int wait = 100;
    for(int xi = objeto.getEscalaX();xi<XMax;xi++){
        objeto.setEscalaX(xi);
        glLoadIdentity();
        gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
        updateGL();
        Sleep(wait);
    }
        for(int yi = objeto.getEscalaY();yi<YMax;yi++){
            objeto.setEscalaY(yi);
            glLoadIdentity();
            gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
            updateGL();
            Sleep(wait);
        }
            for(int zi = objeto.getEscalaZ();zi<ZMax;zi++){
                objeto.setEscalaZ(zi);
                glLoadIdentity();
                gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
                updateGL();
                Sleep(wait);
            }
}


void MeuPainelOpenGL::desenharPlanoX()
{
    planoX = !planoX;
    updateGL();
}
void MeuPainelOpenGL::desenharPlanoY()
{
    planoY = !planoY;
    updateGL();
}
void MeuPainelOpenGL::desenharPlanoZ()
{
    planoZ = !planoZ;
    updateGL();
}

void MeuPainelOpenGL::addRotacaoX()
{
    exibirTransformacao("Rotacao X");
    transformacoes[posicaoTransformacao].setTransformacao(1);
    transformacoes[posicaoTransformacao].setValorRotacao(rotacaoX);
    posicaoTransformacao++;
}

void MeuPainelOpenGL::addRotacaoY()
{
      exibirTransformacao("Rotacao Y");
    transformacoes[posicaoTransformacao].setTransformacao(2);
    transformacoes[posicaoTransformacao].setValorRotacao(rotacaoY);
    posicaoTransformacao++;
}

void MeuPainelOpenGL::addRotacaoZ()
{
    exibirTransformacao("Rotacao Z");
    transformacoes[posicaoTransformacao].setTransformacao(3);
    transformacoes[posicaoTransformacao].setValorRotacao(rotacaoZ);
    posicaoTransformacao++;
}

void MeuPainelOpenGL::addTranslacao()
{
    exibirTransformacao("Translacao");
    transformacoes[posicaoTransformacao].setTransformacao(4);
     transformacoes[posicaoTransformacao].setValorRotacao(0);
     transformacoes[posicaoTransformacao].setValorX(translacaoX);
     transformacoes[posicaoTransformacao].setValorY(translacaoY);
     transformacoes[posicaoTransformacao].setValorZ(translacaoZ);    
    posicaoTransformacao++;
}

void MeuPainelOpenGL::addEscala()
{
    exibirTransformacao("Escala");
    transformacoes[posicaoTransformacao].setTransformacao(5);
     transformacoes[posicaoTransformacao].setValorRotacao(0);
     transformacoes[posicaoTransformacao].setX(escalaX);
     transformacoes[posicaoTransformacao].setY(escalaY);
     transformacoes[posicaoTransformacao].setZ(escalaZ);
     transformacoes[posicaoTransformacao].setValorX(0);
     transformacoes[posicaoTransformacao].setValorY(0);
     transformacoes[posicaoTransformacao].setValorZ(0);

    posicaoTransformacao++;
}

void MeuPainelOpenGL::limparTransformacoes()
{
    posicaoTransformacao = 0;
    posicaoAnimacao = -1;
    emit setT1("Vazio");
    emit setT2("Vazio");
    emit setT3("Vazio");
    emit setT4("Vazio");
    emit setT5("Vazio");
    glLoadIdentity();
    gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
    updateGL();
}

void MeuPainelOpenGL::animacaoGeral()
{

   objeto.setRotacaoX(0);
   objeto.setRotacaoY(0);
   objeto.setRotacaoZ(0);

   objeto.setTranslacaoX(0);
   objeto.setTranslacaoY(0);
   objeto.setTranslacaoZ(0);

   objeto.setEscalaX(1.0);
   objeto.setEscalaY(1.0);
   objeto.setEscalaZ(1.0);

    for(int i=0;i<posicaoTransformacao;i++){
        posicaoAnimacao = i;
        if(transformacoes[i].getTransformacao() == 1){
            animacaoRotacaoX(transformacoes[i].getValorRotacao());
        }else if(transformacoes[i].getTransformacao() == 2){
             animacaoRotacaoY(transformacoes[i].getValorRotacao());
        }else if(transformacoes[i].getTransformacao() == 3){
             animacaoRotacaoZ(transformacoes[i].getValorRotacao());
        }else if(transformacoes[i].getTransformacao() == 4){
             animacaoTranslacao(transformacoes[i].getValorX(),transformacoes[i].getValorY(),transformacoes[i].getValorZ());
        }else if(transformacoes[i].getTransformacao() == 5){
             animacaoEscala(transformacoes[i].getX(),transformacoes[i].getY(),transformacoes[i].getZ());
        }
    }

}



void MeuPainelOpenGL::MoveVisao()
{
    glLoadIdentity();
    gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),getCameraX(), getCameraY(), getCameraZ(), getUpX(), getUpY(), getUpZ());
    updateGL();
}
void MeuPainelOpenGL::Plano()
{

    if(planoX){
        for(int x = 1; x<getMalha();x++){
            glBegin(GL_LINES);

                 glColor3f(1,0,0);
                    // linha
                    glVertex3f(0,x,0);
                    glVertex3f(getMalha(),x,0);
                    // coluna
                    glVertex3f(x,0,0);
                    glVertex3f(x,getMalha(),0);
            glEnd();
        }
    }
    if(planoY){
        for(int x = 1; x<getMalha();x++){
            glBegin(GL_LINES);

                glColor3f(1,1,0);
                    // linha
                    glVertex3f(0,x,0);
                    glVertex3f(0,x,getMalha());
                    // coluna
                    glVertex3f(0,0,x);
                    glVertex3f(0,getMalha(),x);


            glEnd();
        }
    }
    if(planoZ){
        for(int x = 1; x<getMalha();x++){
            glBegin(GL_LINES);
                 glColor3f(0,0,1);

                    //linha
                    glVertex3f(x,0,0);
                    glVertex3f(x,0,getMalha());
                    // coluna
                    glVertex3f(0,0,x);
                    glVertex3f(getMalha(),0,x);
            glEnd();
        }
    }
}
void MeuPainelOpenGL::initializeGL()
{
    glClearColor(.0f, .0f, .0f, .0f);
    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);

    glHint(GL_LINE_SMOOTH_HINT, GL_SMOOTH);
    configurarLuz();
}
void MeuPainelOpenGL::resizeGL(int w, int h)
{

    if(w>h){
        //caso a largura seja maior, o x deve começar na metade da difereça entre a largura e a altura.
        glViewport((w-h)/2,-10, h, h);
    }else{
        //caso a altura seja maior, o y deve começar na metade da difereça entre a altura e a largura  .
        glViewport(-10, (h-w)/2, w, w);
    }

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(15,w/h,0.1f,100);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();


   gluLookAt(getVisaoX(), getVisaoY(), getVisaoZ(),
             getCameraX(),getCameraY(), getCameraZ(),
             getUpX(), getUpY(), getUpZ());

}
void MeuPainelOpenGL::configurarLuz(){
        GLfloat luzAmbiente[4]={0.2,0.2,0.2,1.0};
        GLfloat luzDifusa[4]={0.7,0.7,0.7,1.0};	   // "cor"
        GLfloat luzEspecular[4]={1.0, 1.0, 1.0, 1.0};// "brilho"
        GLfloat posicaoLuz[4]={50.0, 50.0, 50.0, 1.0};

        // Capacidade de brilho do material
        GLfloat especularidade[4]={1.0,1.0,1.0,1.0};
        GLint especMaterial = 60;

        // Especifica que a cor de fundo da janela será preta
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        // Habilita o modelo de colorização de Gouraud
        glShadeModel(GL_SMOOTH);

        // Define a refletância do material
        glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
        // Define a concentração do brilho
        glMateriali(GL_FRONT,GL_SHININESS,especMaterial);

        // Ativa o uso da luz ambiente
        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

        // Define os parâmetros da luz de número 0
        glLightfv(GL_LIGHT0, GL_AMBIENT, luzAmbiente);
        glLightfv(GL_LIGHT0, GL_DIFFUSE, luzDifusa );
        glLightfv(GL_LIGHT0, GL_SPECULAR, luzEspecular );
        glLightfv(GL_LIGHT0, GL_POSITION, posicaoLuz );

        // Habilita a definição da cor do material a partir da cor corrente
        glEnable(GL_COLOR_MATERIAL);
        //Habilita o uso de iluminação
        glEnable(GL_LIGHTING);
        // Habilita a luz de número 0
        glEnable(GL_LIGHT0);
        // Habilita o depth-buffering
        glEnable(GL_DEPTH_TEST);
}
void MeuPainelOpenGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);   
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);

    Plano();

    for(int i=posicaoAnimacao;i>=0;i--){
        if(posicaoAnimacao == i){
            if(transformacoes[i].getTransformacao() == 1){
                glRotatef(objeto.getRotacaoX(),1,0,0);
            }else if(transformacoes[i].getTransformacao() == 2){
                glRotatef(objeto.getRotacaoY(),0,1,0);
            }else if(transformacoes[i].getTransformacao() == 3){
                glRotatef(objeto.getRotacaoZ(),0,0,1);
            }else if(transformacoes[i].getTransformacao() == 4){
                glTranslatef(objeto.getTranslacaoX(),objeto.getTranslacaoY(),objeto.getTranslacaoZ());
            }else if(transformacoes[i].getTransformacao() == 5){
                glScalef(objeto.getEscalaX(),objeto.getEscalaY(),objeto.getEscalaZ());
            }
        }else{
            if(transformacoes[i].getTransformacao() == 1){
                glRotatef(transformacoes[i].getValorRotacao(),1,0,0);
            }else if(transformacoes[i].getTransformacao() == 2){
                glRotatef(transformacoes[i].getValorRotacao(),0,1,0);
            }else if(transformacoes[i].getTransformacao() == 3){
                glRotatef(transformacoes[i].getValorRotacao(),0,0,1);
            }
        }

    }






    float colorBlue[4]       = { 0.0, 0.2, 1.0, 0.6 };
    float colorNone[4]       = { 0.0, 0.0, 0.0, 0.0 };
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glColor4fv(colorBlue);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, colorBlue);
    glMaterialfv(GL_FRONT, GL_SPECULAR, colorNone);
    glBindTexture(GL_TEXTURE_2D, 1);


    //plano xy


    glBegin(GL_QUADS);

    //Face posterior
    glNormal3i(0,0,1);
    glVertex3f( 0.0f, 0.0f, 0.0f);
    glVertex3f( 0.0f, getTamanho(), 0.0f);
    glVertex3f( getTamanho(), getTamanho(), 0.0f);
    glVertex3f( getTamanho(), 0.0f, 0.0f);

    glEnd();

    glBegin(GL_QUADS);

    //Face frontal
    glNormal3i(0,0,-1);
    glVertex3f( getTamanho(), getTamanho(), getTamanho());
    glVertex3f( getTamanho(), 0.0f, getTamanho());
    glVertex3f( 0.0f, 0.0f, getTamanho());
    glVertex3f( 0.0f, getTamanho(), getTamanho());

    glEnd();

    glBegin(GL_QUADS);

    // zy x=baseSize
    glNormal3f(1,0,0);
    glVertex3f( getTamanho(), 0.0f, 0.0f);
    glVertex3f( getTamanho(), getTamanho(), 0.0f);
    glVertex3f( getTamanho(), getTamanho(), getTamanho());
    glVertex3f( getTamanho(), 0.0f, getTamanho());

    glEnd();

    glBegin(GL_QUADS);

    //  zy x=0
    glNormal3f(-1,0,0);
    glVertex3f( 0.0f, 0.0f, getTamanho());
    glVertex3f( 0.0f, getTamanho(), getTamanho());
    glVertex3f( 0.0f, getTamanho(), 0.0f);
    glVertex3f( 0.0f, 0.0f, 0.0f);

    glEnd();

    glBegin(GL_QUADS);

    // Cima
    glNormal3f(0,1,0);
    glVertex3f( 0.0f, getTamanho(), 0.0f);
    glVertex3f( getTamanho(), getTamanho(), 0.0f);
    glVertex3f( getTamanho(), getTamanho(), getTamanho());
    glVertex3f( 0.0f, getTamanho(), getTamanho());

    glEnd();

    glBegin(GL_QUADS);

    // Baixo
    glNormal3f(0,-1,0);
    glVertex3f(0.0f, 0.0f, 0.0f);
    glVertex3f(getTamanho(), 0.0f, 0.0f);
    glVertex3f(getTamanho(), 0.0f, getTamanho());
    glVertex3f(0.0f, 0.0f, getTamanho());

    glEnd();

    glFlush();

}
