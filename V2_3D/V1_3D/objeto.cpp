#include "objeto.h"

Objeto::Objeto()
{
    rotacaoX = 0;
    rotacaoY = 0;
    rotacaoZ = 0;

    translacaoX = 0;
    translacaoY = 0;
    translacaoZ = 0;

    escalaX = 1.0;
    escalaY = 1.0;
    escalaZ = 1.0;
}

int Objeto::getRotacaoX()
{
    return rotacaoX;
}

void Objeto::setRotacaoX(int value)
{
    rotacaoX = value;
}

int Objeto::getRotacaoY()
{
    return rotacaoY;
}

void Objeto::setRotacaoY(int value)
{
    rotacaoY = value;
}

int Objeto::getRotacaoZ()
{
    return rotacaoZ;
}

void Objeto::setRotacaoZ(int value)
{
    rotacaoZ = value;
}

int Objeto::getTranslacaoX() const
{
    return translacaoX;
}

void Objeto::setTranslacaoX(int value)
{
    translacaoX = value;
}

int Objeto::getTranslacaoY() const
{
    return translacaoY;
}

void Objeto::setTranslacaoY(int value)
{
    translacaoY = value;
}

int Objeto::getTranslacaoZ() const
{
    return translacaoZ;
}

void Objeto::setTranslacaoZ(int value)
{
    translacaoZ = value;
}

int Objeto::getEscalaX() const
{
    return escalaX;
}

void Objeto::setEscalaX(double value)
{
    escalaX = value;
}

int Objeto::getEscalaY() const
{
    return escalaY;
}

void Objeto::setEscalaY(double value)
{
    escalaY = value;
}

int Objeto::getEscalaZ() const
{
    return escalaZ;
}

void Objeto::setEscalaZ(double value)
{
    escalaZ = value;
}
