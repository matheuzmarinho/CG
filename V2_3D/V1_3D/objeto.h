#ifndef OBJETO_H
#define OBJETO_H


class Objeto
{
public:
    Objeto();


    int getRotacaoX();
    void setRotacaoX(int value);

    int getRotacaoY();
    void setRotacaoY(int value);

    int getRotacaoZ();
    void setRotacaoZ(int value);

    int getTranslacaoX() const;
    void setTranslacaoX(int value);

    int getTranslacaoY() const;
    void setTranslacaoY(int value);

    int getTranslacaoZ() const;
    void setTranslacaoZ(int value);

    int getEscalaX() const;
    void setEscalaX(double value);

    int getEscalaY() const;
    void setEscalaY(double value);

    int getEscalaZ() const;
    void setEscalaZ(double value);

private:
    int rotacaoX;
    int rotacaoY;
    int rotacaoZ;
    int translacaoX;
    int translacaoY;
    int translacaoZ;
    double escalaX;
    double escalaY;
    double escalaZ;

};

#endif // OBJETO_H
