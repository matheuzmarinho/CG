#ifndef OBJETOTRANSFORMACAO_H
#define OBJETOTRANSFORMACAO_H


class ObjetoTransformacao
{
public:
    ObjetoTransformacao();

    int getTransformacao() const;
    void setTransformacao(int value);

    int getValorX() const;
    void setValorX(int value);

    int getValorY() const;
    void setValorY(int value);

    int getValorZ() const;
    void setValorZ(int value);

    int getValorRotacao() const;
    void setValorRotacao(int value);

    double getX() const;
    void setX(double value);

    double getY() const;
    void setY(double value);

    double getZ() const;
    void setZ(double value);

private:
    int transformacao;
    int valorRotacao;
    int valorX;
    int valorY;
    int valorZ;
    double X;
    double Y;
    double Z;
};

#endif // OBJETOTRANSFORMACAO_H
