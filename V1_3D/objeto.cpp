#include "objeto.h"

Objeto::Objeto()
{
    rotacaoX = 0;
    rotacaoY = 0;
    rotacaoZ = 0;
}

int Objeto::getRotacaoX()
{
    return rotacaoX;
}

void Objeto::setRotacaoX(int value)
{
    rotacaoX = value;
}

int Objeto::getRotacaoY()
{
    return rotacaoY;
}

void Objeto::setRotacaoY(int value)
{
    rotacaoY = value;
}

int Objeto::getRotacaoZ()
{
    return rotacaoZ;
}

void Objeto::setRotacaoZ(int value)
{
    rotacaoZ = value;
}
