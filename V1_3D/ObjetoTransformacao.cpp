#include "ObjetoTransformacao.h"

ObjetoTransformacao::ObjetoTransformacao()
{

}

int ObjetoTransformacao::getTransformacao() const
{
    return transformacao;
}

void ObjetoTransformacao::setTransformacao(int value)
{
    transformacao = value;
}

int ObjetoTransformacao::getValorX() const
{
    return valorX;
}

void ObjetoTransformacao::setValorX(int value)
{
    valorX = value;
}

int ObjetoTransformacao::getValorY() const
{
    return valorY;
}

void ObjetoTransformacao::setValorY(int value)
{
    valorY = value;
}

int ObjetoTransformacao::getValorZ() const
{
    return valorZ;
}

void ObjetoTransformacao::setValorZ(int value)
{
    valorZ = value;
}

int ObjetoTransformacao::getValorRotacao() const
{
    return valorRotacao;
}

void ObjetoTransformacao::setValorRotacao(int value)
{
    valorRotacao = value;
}
