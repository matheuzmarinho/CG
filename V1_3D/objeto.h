#ifndef OBJETO_H
#define OBJETO_H


class Objeto
{
public:
    Objeto();


    int getRotacaoX();
    void setRotacaoX(int value);

    int getRotacaoY();
    void setRotacaoY(int value);

    int getRotacaoZ();
    void setRotacaoZ(int value);

private:
    int rotacaoX;
    int rotacaoY;
    int rotacaoZ;
};

#endif // OBJETO_H
