#ifndef OBJETOTRANSFORMACAO_H
#define OBJETOTRANSFORMACAO_H


class ObjetoTransformacao
{
public:
    ObjetoTransformacao();

    int getTransformacao() const;
    void setTransformacao(int value);

    int getValorX() const;
    void setValorX(int value);

    int getValorY() const;
    void setValorY(int value);

    int getValorZ() const;
    void setValorZ(int value);

    int getValorRotacao() const;
    void setValorRotacao(int value);

private:
    int transformacao;
    int valorRotacao;
    int valorX;
    int valorY;
    int valorZ;
};

#endif // OBJETOTRANSFORMACAO_H
