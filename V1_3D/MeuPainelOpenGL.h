#ifndef MEUPAINELOPENGL_H
#define MEUPAINELOPENGL_H

#include <QWidget>
#include <QGLWidget>
#include "objeto.h"
#include "ObjetoTransformacao.h"

class MeuPainelOpenGL : public QGLWidget
{
    Q_OBJECT
public:
    explicit MeuPainelOpenGL(QWidget *parent = 0);

    int getVisaoX() const;
    int getVisaoY() const;
    int getVisaoZ() const;


    int getCameraX() const;
    int getCameraY() const;
    int getCameraZ() const;


    int getMalha() const;

    int getUpX() const;
    int getUpY() const;
    int getUpZ() const;

    float getTamanho() const;

    float getRotacaoX() const;
    float getRotacaoY() const;
    float getRotacaoZ() const;


    bool getPlanoX() const;
    void setPlanoX(bool value);

    bool getPlanoY() const;
    void setPlanoY(bool value);

    bool getPlanoZ() const;
    void setPlanoZ(bool value);

    float getTranslacaoX() const;
    float getTranslacaoY() const;
    float getTranslacaoZ() const;


private:
    Objeto objeto;
    float translacaoX;
    float translacaoY;
    float translacaoZ;

    float rotacaoX;
    float rotacaoY;
    float rotacaoZ;
    float rotacaoXObjeto;
    float rotacaoYObjeto;
    float rotacaoZObjeto;

    int visaoX;
    int visaoY;
    int visaoZ;
    int CameraX;
    int CameraY;
    int CameraZ;
    bool planoX;
    bool planoY;
    bool planoZ;

    int UpX;
    int UpY;
    int UpZ;

    int malha;

    float tamanho;
    ObjetoTransformacao transformacoes[50];
    int posicaoTransformacao;
    int posicaoAnimacao;

     void exibirTransformacao(QString transformacao);

public slots:
    void setRotacaoX(int valor);
    void setRotacaoZ(int valor);
    void setRotacaoY(int valor);

    void setTranslacaoZ(float value);
    void setTranslacaoY(float value);
    void setTranslacaoX(float value);


    void animacaoRotacaoX(int valor);
    void animacaoRotacaoY(int valor);
    void animacaoRotacaoZ(int valor);


    void desenharPlanoX();
    void desenharPlanoY();
    void desenharPlanoZ();


    void addRotacaoX();
    void addRotacaoY();
    void addRotacaoZ();




    void limparTransformacoes();
    void animacaoGeral();

    void setVisaoX(int valor);
    void setVisaoY(int valor);
    void setVisaoZ(int valor);

    void setCameraZ(int valor);
    void setCameraY(int valor);
    void setCameraX(int valor);

    void setUpZ(int valor);
    void setUpY(int valor);
    void setUpX(int valor);

    void setTamanho(float valor);

protected:
    void initializeGL();
    void resizeGL(int w , int h);
    void paintGL();

    void MoveVisao();
    void configurarLuz();
    void Plano();




signals:
    void setT1(QString str);
    void setT2(QString str);
    void setT3(QString str);
    void setT4(QString str);
    void setT5(QString str);

public slots:
};

#endif // MEUPAINELOPENGL_H
