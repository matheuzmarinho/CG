#include "Reta.h"
#include "GL/glu.h"

Reta::Reta()
{
    X1 = 0;
    X2 = 0;
    Y1 = 0;
    Y2 = 0;
}

void Reta::draw(int X1, int X2, int Y1, int Y2)
{
    glBegin(GL_LINES);
        glVertex2f(X1, X2);
        glVertex2f(Y1, Y2);
    glEnd();
}

void Reta::setX1(int n) {
    X1 = n;
}

void Reta::setX2(int n) {
    X2 = n;
}

void Reta::setY1(int n) {
    Y1 = n;
}

void Reta::setY2(int n) {
    Y2 = n;
}

int Reta::getX1() {
    return X1;
}

int Reta::getX2() {
    return X2;
}


int Reta::getY1() {
    return Y1;
}

int Reta::getY2() {
    return Y2;
}
