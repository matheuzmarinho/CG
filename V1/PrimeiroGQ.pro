#-------------------------------------------------
#
# Project created by QtCreator 2018-03-14T16:49:09
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = PrimeiroGQ
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        JanelaPrincipal.cpp \
    MeuPainelOpenGL.cpp \
    Reta.cpp

HEADERS += \
        JanelaPrincipal.h \
    MeuPainelOpenGL.h \
    Reta.h

FORMS += \
        JanelaPrincipal.ui
