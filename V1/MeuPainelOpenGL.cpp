#include "MeuPainelOpenGL.h"
#include "Reta.h"

MeuPainelOpenGL::MeuPainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{

}

void MeuPainelOpenGL::initializeGL()
{
qglClearColor(Qt::white);
}

void MeuPainelOpenGL::resizeGL(int w, int h)
{
glViewport(0, 0, w, h);
}

void MeuPainelOpenGL::paintGL()
{
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

glColor3f(1,0,0);//glBegin(GL_TRIANGLES);    glVertex2f(-1,-1);    glVertex2f(0,1);    glVertex2f(1,-1);glEnd();

    reta.draw(reta.getX1(),reta.getX2(),reta.getY1(),reta.getY2());

}

void MeuPainelOpenGL::setX1(int n)
{
    reta.setX1(n);
    updateGL();
}

void MeuPainelOpenGL::setX2(int n)
{
      reta.setX2(n);
      updateGL();
}

void MeuPainelOpenGL::setY1(int n)
{
      reta.setY1(n);
      updateGL();
}

void MeuPainelOpenGL::setY2(int n)
{
      reta.setY2(n);
      updateGL();
}


