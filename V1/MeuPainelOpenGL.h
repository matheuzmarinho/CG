#ifndef MEUPAINELOPENGL_H
#define MEUPAINELOPENGL_H

#include <QWidget>
#include <QGLWidget>
#include "Reta.h"

class MeuPainelOpenGL : public QGLWidget
{
    Q_OBJECT
public:
    explicit MeuPainelOpenGL(QWidget *parent = 0);


protected:
    void initializeGL();
    void resizeGL(int w , int h);
    void paintGL();


signals:

public slots:
    void setX1(int n);
    void setX2(int n);
    void setY1(int n);
    void setY2(int n);

private:
    Reta reta;

};

#endif // MEUPAINELOPENGL_H
