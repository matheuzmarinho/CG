#ifndef RETA_H
#define RETA_H
#include <string.h>


class Reta
{


public:
    Reta();
    void draw(int X1, int X2, int Y1, int Y2);


    void setX1(int n);
    void setX2(int n);
    void setY1(int n);
    void setY2(int n);
    int getX1();
    int getY1();
    int getX2();
    int getY2();

private:
     int X1, X2, Y1, Y2;

};

#endif // RETA_H
